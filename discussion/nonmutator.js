// console.log("Hello World")
//Non-mutator methods

/*
	These are functions that do not modify an array after they have been created
*/

console.log("%c Non-Mutator Methods:", 'font-weight: bold;');
let countries = ['US', 'PH','CAN','SG','TH','PH','FR','DE'];
console.log(countries);
console.log("");

// indexOf()
/*
	Returns the index number of the first matching element found in an array
	if no match has been found, result will be -1
	search process will be done from the first element proceeding to the last element.
	Syntax:
		arrayName.indexOf(searchValue);
		arrayName.indexOf(serchValue, fromIndex);
*/

console.log("%c indexOf Methods:", 'background: #89CFF0');
let firstIndex = countries.indexOf('PH');
console.log("Result of indexOf method: "+firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log("Result of indexOf method: " + invalidCountry);


// lastIndexOf()
/*
	Returns the index number of the last matching element found in an array
	If no match has been found, result will be -1
	Search process will be done from the last element proceeding to the first element
	Syntax:
		arrayName.indexOf(searchValue);
		arrayName.indexOf(searchValue, fromIndex);
*/

console.log("%c lastIndexOf Methods:", 'background: #89CFF0');
//Getting the index number starting from the last element
let lastIndex = countries.lastIndexOf('PH')
console.log("Result of lastIndexOf Method: " + lastIndex);

//Getting the index number starting from a specified index
let lastIndexStart = countries.lastIndexOf('PH', 4);
console.log("Result of lastIndexOf Method: " + lastIndexStart);
console.log("")

//slice()
/*
	Slices elements from an array and returns a new array
	Syntax:
		arrayName.slice(startingIndex);
		arrayName.slice(startingIndex, endingIndex);
*/
console.log("%c slice Methods:", 'background: #89CFF0');
//Slicing off elements from a specified index to the last element
let sliceArrayA = countries.slice(2);
console.log("Result from slice method: ");
console.log(sliceArrayA);

//slicing off elements from a specified index to another index
let sliceArrayB = countries.slice(2,4);
console.log("Result of slice method: ");
console.log(sliceArrayB);

//Slicing off elements starting from the last element of an array
//the argument with negative number means that the slice starts from the end and counts with given value
let sliceArrayC = countries.slice(-3);
let sliceArrayD = countries.slice(-4, -1);
let sliceArrayE = countries.slice(-6, -2);
console.log("Result from slice method: ");
console.log(sliceArrayC);
console.log(sliceArrayD);
console.log(sliceArrayE);
console.log("")


//toString()
/*
	Returns an array as a string separated by commas
	Syntax:
		arrayName.toString();
*/
console.log("%c toString Methods:", 'background: #89CFF0');

let stringArray = countries.toString();
console.log("Result of toString Method: ");
console.log(stringArray);

// concat()
/*
	Combines two arrays and returns the combined result
	Syntax:
		arrayA.concat(arrayB)
		arrayA.concat(elementA, elementB)

*/

console.log("%c concat() Method:", 'background: #89CFF0');
let tasksArrayA = ["drink HTML", "eat javascript"];
let tasksArrayB = ["inhale CSS", "breathe sass"];
let tasksArrayC = ["get git", "be node"];

let tasks = tasksArrayA.concat(tasksArrayB);
console.log("Result from concat method: ");
console.log(tasks);

//Combining multiple arrays
console.log("Result from concat method: ");
let allTasks = tasksArrayA.concat(tasksArrayB, tasksArrayC);
console.log(allTasks);

//Combining arrays with elements
let combinedTasks = tasksArrayA.concat('smell expresse', 'throw react');
console.log("Result from concat method: ");
console.log(combinedTasks);


// join()
/*
	Returns an array as a string separated by specified string
	Syntax:
		arrayName.join("Separator string");
*/

console.log("%c join() Method:", 'background: #89CFF0');
let users = ['John','Jane','Joe','Robert'];
let users2 = [3,4,5,7];
console.log(users.join());
console.log(users.join(""));
console.log(users.join(' - '));
console.log("")



console.warn("--end of non-mutator methods");


console.log("%c Iteration Method:", 'font-weight: bold;');


//Iteration methods
/*
	These are loops designed to perform repetitive tasks on arrays
	Useful for manipulating array data resulting in complex tasks.
*/

// foreach()
/*
	similar to the for loop that iterates on each array element
	array iteration usually works with a function as an argument
	Syntax:
		arrayName.forEach(function(individualElement){statement})
*/

allTasks.forEach(function(task){console.log(task)});
console.log("")

//Using forEach with conditional statements
console.log("%c Filtered forEach Method:", 'text-decoration: underline;');

let filteredTasks = [];


allTasks.forEach(function(task){
	if(task.length > 10){
		console.log(task);
		filteredTasks.push(task);
	}
})

console.log("Result of filtered task:");
console.log(filteredTasks);
console.log("Original Array:")
console.log(allTasks)

console.log("")

//map()
/*
	It iterates of each element and returns new array with different values depending on the result of the function's operation
	This is useful for performing tasks where mutating/changing the elements are required
	Unlike the forEach method, the map method requires the use of a "return" statement in oreder to create another array with the performed operation
	Syntax:
		let/const resultArray = arrayName.map(function(individualElement))
*/
console.log("%c Map Method:", 'background: #89CFF0');

let numbers = [1,2,3,4,5];
console.log("Before the map method: " + numbers);

let numberMap = numbers.map(function(number){
	return number * number
})
console.log("Resul of map method:")
console.log(numberMap);
console.log("")
console.log(numbers)


// every()
/*
	Checks if all elements in an array meet the given condition
	This is useful for validating data stored in arrays especially when dealing with large amounts of data
	Returns the true value if all elements meet the condition and false if otherwise
	Syntax:
		let/const resultArray = arrayName.every(function(element){
			return expression / condition
		})
*/


let allValid = numbers.every(function(number){
	return (number < 3)
});
console.log("Result of every method: ");
console.log(allValid);

// some()
/*
	Checks if at least one element in the array meets the given condition
	Returns a true value if at least on element meets the condition and false if otherwise

	Syntax:
		let/const resultArray = arrayName.some(function(element){
			return expression/condition
		})
*/

console.log("%c some() Method:", 'background: #89CFF0');
let someValid = numbers.some(function(number){
	return (number < 2);
});

console.log("Result of some() method:")
console.log(someValid)

//Combining the returned result from the every/some method may be used in other statements to perform consecutive
if(someValid){
	console.log("Some numbers in the array are greater than 2");
}

console.log("")

//filter()
/*
	Returns new array that contains elements which meets the given condition
	Returns an empty array if no elements are found
	Useful for filtering array elements with a given condition and shortens the syntax compared to using other array iteration methods.
	Syntax:
		let resultArray = arrayName.filter(function(element){
			return expression/condition
		})
*/

	
console.log("%c filter() Method:", 'background: #89CFF0');
let filterValid = numbers.filter(function(number){
		return (number<3)
});

console.log("Result of the filter method:")
console.log(filterValid);



let nothingFound = numbers.filter(function(number) {
	return (number = 0)
});
console.log(nothingFound)

// Filtering using forEach
let filteredNumber = [];

numbers.forEach(function(number){
	console.log(number)
	if(number < 3){
		filteredNumber.push(number)
	}
});
console.log("Result of filtered method:")
console.log(filteredNumber);

//includes() method
/*
	methods can be chained by using them one after another
*/
console.log("%c includes() Method:", 'background: #89CFF0');



let products = ['Mouse','Keyboard','Laptop', 'Monitor'];
let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes("a");
})
console.log(filteredProducts);
console.log("");



// reduce()
/*
	Evaluates statements from left to right and returns or reduces the array into a single value
	Syntax:
		let/const resultArray = arrayName.reduce(function(accumulator, currentValue){
			return expression/condition
		})
	-the accumulator parameter in the function stores the result for every iteration of the loop
	-the current value parameter is the next element in the array that is evaluated in each iteration of the loop
	-how the "reduce" method works:
		1. the first/result element in the array is stored in the "accumulator" parameter.
		2. the next element in the array is stored in the "currentValue"
		3. The loop repeats steps 1 to 3 until all elements have been worked on.

	my explanation:
	reduce method loops through each array element where in you can return a reduced data using condition/expression
*/

let iteration = 0;

let reducedArray = numbers.reduce(function(x,y){
	console.warn("current iteration: " + ++iteration)
	console.log("accumulator: " + x)
	console.log("currentValue: " + y)

	// The operation to reduce the array into a single value
	return x + y
});

console.log("Result of Reduce method: " + reducedArray);


//Reducing string arrays
let list = ['Hello','Again','World'];

let reducedJoin = list.reduce(function(x, y){
	return x+ ' ' +y
});
console.log("Result of reduce method: " + reducedJoin);































