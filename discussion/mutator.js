// console.log("Hello World")

// Array Methods
/*
	for array methords, we have the MUTATOR and NON-MUTATOR methods
*/

// Mutator Methods
/*
	Mutator methods are functions that change an array after they're created.
	These methods manipulate the original array. Performing tasks such as adding and removing elements
	-push(), pop(), unshift(), shift(), splice(), sort(), reverse()
*/
console.log('%c Mutator Methods', 'font-weight: bold;')
let fruits = ["Apple", "Orange", "Kiwi", "Dragon Fruit"];
console.log(fruits);
console.log("");

// push()
/*
	Adds in the end of the array and returns the array's NEW LENGTH after pushing
	syntax:
		arrayname.push()
*/

console.log("%c Push Method:", "background: #89CFF0")
let fruitsLength = fruits.push('Mango');
console.log(fruitsLength);
console.log("Mutated array from push method: ")
console.log(fruits);

//Push multiple elements into an array
fruits.push("Avocado", "Guava");
console.log("Mutated array from push method: (multiple values)")
console.log(fruits)

// pop()
/*
	Removes the last element of an array and returns the REMOVED ELEMENT
	Syntax:
		arrayName.pop();
*/
console.log("%c Pop Method:", "background: #89CFF0");
let removedFruit = fruits.pop();
console.log(removedFruit);
console.log("Mutated array from pop method:")
console.log(fruits);
console.log("");

// unshift()
/*
	"Adds" at the start of an array and returns the resulting length of the array 
	["x",0,0]
	Syntax:
		arrayName.unshift("elementA");
		arrayName.unshift('elementA', 'elementB')
*/

console.log("%c Unshift Method:", "background: #89CFF0")
let trialReturn = fruits.unshift('Lime', 'Banana');
console.log(trialReturn);
console.log("Mutated array from unshift method:");
console.log(fruits);
console.log("");

//shift()
/*
	Removes element at the start of an array and returns the REMOVED ELEMENT
	Syntax:
		arrayName.shift()
*/

console.log("%c Shift Method:", 'background: #89CFF0')
let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method:");
console.log(fruits);
console.log("");

// splice()
/*
	Simultaneously removes element from a specified number and adds elements and returns the removed elements
	Syntax:
		arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/

console.log("%c Splice Method:", 'background: #89CFF0');
let trial2 = fruits.splice(3,0,"Lime","Cherry");
console.log(trial2);
console.log("Mutated array from splice method:");
console.log(fruits);
console.log("");

// sort()
/*
	rearranges the array elements in Alphanumeric order
	Syntax:
		arrayName.sort();
*/

console.log("%c Sort Method:", 'background: #89CFF0');
let trial3 = fruits.sort();
console.log(trial3)
console.log("Mutated array from sort method: ");
console.log(fruits);
console.log("")

// reverse()
/*
	Reverses the order of array elements
	Syntax: 
		arrayName.reverse()
*/


console.log("%c Unshift Method:", "background: #89CFF0");
let trial4 = fruits.reverse();
console.log(trial4);
console.log("Mutated array from reverse method:");
console.log(fruits);
console.log("");
console.warn(" end of mutator methods");


















